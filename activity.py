# -----------> ACTIVITY SOLUTION <-----------
# Part 1 w/ Stretch Goals
while True: 
    try :
        year = int(input("Please input a year:\n"))
    except ValueError :
        print("Strings are not allowed. Please enter a number.")
        continue
    if year <= 0 :
        print("Not allowed to input a number that is less than or equal to 0.")
    else: 
        break
if year % 4 == 0 :
    print(f"{year} is a leap year.")
else :
    print(f"{year} is not a leap year.")

# Part 2
row = int(input("Enter number of rows: "))
column = int(input("Enter number of columns: "))
for i in range(row) :
    print('* ' * column)
